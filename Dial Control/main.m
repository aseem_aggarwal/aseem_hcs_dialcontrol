//
//  main.m
//  Dial Control
//
//  Created by Aseem Aggarwal on 2/20/14.
//  Copyright (c) 2014 Aseem Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DialControlAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DialControlAppDelegate class]));
    }
}
